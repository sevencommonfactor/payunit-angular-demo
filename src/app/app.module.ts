import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxPayunitModule } from 'ngx-payunit';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DonateComponent } from './Components/donate/donate.component';
import { SucessComponent } from './Components/sucess/sucess.component';

@NgModule({
  declarations: [
    AppComponent,
    DonateComponent,
    SucessComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPayunitModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
