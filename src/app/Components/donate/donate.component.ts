import { Component, OnInit } from '@angular/core';
import { NgxPayunitComponent } from 'ngx-payunit';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.scss']
})
export class DonateComponent implements OnInit {
  private payunit: NgxPayunitComponent;

  constructor() {
    this.payunit = new NgxPayunitComponent();
  }

  ngOnInit(): void {
    this.initConfig();
  }

  private initConfig(): void {
    let config = {
      apiUsername: 'payunit_**************',
      apiPassword: 'c1968535-*************',
      x_api_key: '8fed05**************',
      mode: 'test',
    };

    let data = {
      return_url: "http://localhost:4200/success",
      notify_url: "",
      description: "Online payment with payunit js sdk",
      purchaseRef: "7d**********",
      total_amount: "500",
      name: "name is unique",
      currency: 'XAF',
    };

    this.payunit.config(config);
    this.payunit.payload(data);
  }

  makePayment() {
    this.payunit.pay();
  }


}
