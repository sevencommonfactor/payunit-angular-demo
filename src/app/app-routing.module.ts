import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DonateComponent } from './Components/donate/donate.component';
import { SucessComponent } from './Components/sucess/sucess.component';

const routes: Routes = [
  {path: '', component: DonateComponent},
  {path: 'success', component: SucessComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
